package com.muhardin.endy.training.automatedtest.aplikasibukutamu;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InputTamuTests {
    
    private static WebDriver wd;
    private static final String URL_APLIKASI = "https://training-selenium-201903.herokuapp.com";
    
    //@BeforeClass
    public static void sebelumSemuaTest(){
        System.out.println("Sebelum semua test dijalankan");
        
        wd = new ChromeDriver();
    }
    
    //@Before
    public void sebelumMasingMasingTest(){
        System.out.println("Sebelum masing-masing test dijalankan");
    }
    
    //@After
    public void setelahMasingMasingTest(){
        System.out.println("Setelah masing-masing test dijalankan");
    }
    
    //@AfterClass
    public static void setelahSemuaTest(){
        System.out.println("Setelah semua test dijalankan");
        wd.quit();
    }
    
    //@Test
    public void testInputTamuNormal(){
        System.out.println("Test input tamu normal");
        wd.get(URL_APLIKASI + "/tamu/form");
    }
    
    @Test
    public void testInputTamuNamaInvalid(){
        System.out.println("Test input tamu nama invalid");
    }
    
    //@Test
    public void testInputTamuFormatEmailSalah(){
        System.out.println("Test input email invalid format");
    }
}
