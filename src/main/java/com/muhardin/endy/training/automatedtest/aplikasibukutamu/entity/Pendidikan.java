package com.muhardin.endy.training.automatedtest.aplikasibukutamu.entity;

public enum Pendidikan {
    SD,SMP,SMA,SMK,D1,D2,D3,D4,S1,S2,S3
}
