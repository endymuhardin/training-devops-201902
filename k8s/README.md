# Deploy Aplikasi ke DigitalOcean Kubernetes #

Persiapan cluster kubernetes di DigitalOcean

1. Create Cluster

    [![create-k8s-cluster](../docs/img/k8s/01-create-k8s-cluster.png)](../docs/img/k8s/01-create-k8s-cluster.png)

    [![02-cluster-capacity](../docs/img/k8s/02-cluster-capacity.png)](../docs/img/k8s/02-cluster-capacity.png)

    [![03-cluster-name](../docs/img/k8s/03-cluster-name.png)](../docs/img/k8s/03-cluster-name.png)

    [![04-getting-started](../docs/img/k8s/04-getting-started.png)](../docs/img/k8s/04-getting-started.png)

2. Download file konfigurasi untuk authentication

    [![05-download-config-file](../docs/img/k8s/05-download-config-file.png)](../docs/img/k8s/05-download-config-file.png)

3. Set file konfigurasi sebagai environment variable

    Windows

        set KUBECONFIG=k8s-training-devops-201902-kubeconfig.yaml
    
    Linux

        export KUBECONFIG=k8s-training-devops-201902-kubeconfig.yaml

4. Cek versi kubernetes yang berjalan di client dan server

    ```
    $ kubectl version  
    Client Version: version.Info{Major:"1", Minor:"15", GitVersion:"v1.15.2", GitCommit:"f6278300bebbb750328ac16ee6dd3aa7d3549568", GitTreeState:"clean", BuildDate:"2019-08-05T16:54:35Z", GoVersion:"go1.12.7", Compiler:"gc", Platform:"darwin/amd64"}
    Server Version: version.Info{Major:"1", Minor:"15", GitVersion:"v1.15.2", GitCommit:"f6278300bebbb750328ac16ee6dd3aa7d3549568", GitTreeState:"clean", BuildDate:"2019-08-05T09:15:22Z", GoVersion:"go1.12.5", Compiler:"gc", Platform:"linux/amd64"}
    ```

5. Cek node yang ada dalam cluster

    ```
    $ kubectl get nodes
    NAME                      STATUS   ROLES    AGE     VERSION
    training-node-pool-rl80   Ready    <none>   4m52s   v1.15.2
    training-node-pool-rl8d   Ready    <none>   4m39s   v1.15.2
    training-node-pool-rl8v   Ready    <none>   4m44s   v1.15.2
    ```

## Deploy Aplikasi ##

1. Deploy Persistent Volume

2. Melihat daftar persistent volume

    ```
    $ kubectl get pv
    NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                 STORAGECLASS       REASON   AGE
    pvc-d4a149ad-c384-443d-a6b0-038e0a5f3080   5Gi        RWO            Delete           Bound    default/mysql-db-pv   do-block-storage            13s
    ```

2. Deploy Database

    ```
    $ kubectl apply -f 02-database-server.yml 
    service/bukutamu-db-service unchanged
    deployment.apps/bukutamu-db created
    ```

3. Melihat status deployment

    ```
    $ kubectl get deployment
    NAME          READY   UP-TO-DATE   AVAILABLE   AGE
    bukutamu-db   0/1     1            0           14s
    ```

4. Melihat status pods

    ```
    $ kubectl get pods      
    NAME                          READY   STATUS   RESTARTS   AGE
    bukutamu-db-f498f8675-w5tpd   0/1     Error    2          58s
    ```

5. Melihat log aplikasi dalam container

    ```
    $ kubectl logs --follow bukutamu-db-f498f8675-w5tpd
    Initializing database
    2019-08-21T02:15:45.374055Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
    2019-08-21T02:15:45.375820Z 0 [ERROR] --initialize specified but the data directory has files in it. Aborting.
    2019-08-21T02:15:45.376112Z 0 [ERROR] Aborting
    ```

6. Deployment database berhasil dijalankan

    ```
    $ kubectl get pods
    NAME                           READY   STATUS    RESTARTS   AGE
    bukutamu-db-78b9445cbf-4dfnq   1/1     Running   0          32s


    $ kubectl logs bukutamu-db-78b9445cbf-4dfnq
    Initializing database
    2019-08-21T02:24:46.105144Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
    2019-08-21T02:24:46.821573Z 0 [Warning] InnoDB: New log files created, LSN=45790
    2019-08-21T02:24:46.974903Z 0 [Warning] InnoDB: Creating foreign key constraint system tables.
    2019-08-21T02:24:47.072281Z 0 [Warning] No existing UUID has been found, so we assume that this is the first time that this server has been started. Generating a new UUID: d83d0849-c3ba-11e9-a5e3-0203596bbf44.
    2019-08-21T02:24:47.099737Z 0 [Warning] Gtid table is not ready to be used. Table 'mysql.gtid_executed' cannot be opened.
    2019-08-21T02:24:47.100595Z 1 [Warning] root@localhost is created with an empty password ! Please consider switching off the --initialize-insecure option.
    Database initialized
    Initializing certificates
    Generating a RSA private key
    .............................................................................+++++
    .........................................................................................+++++
    unable to write 'random state'
    writing new private key to 'ca-key.pem'
    -----
    Generating a RSA private key
    ................................+++++
    ............................................+++++
    unable to write 'random state'
    writing new private key to 'server-key.pem'
    -----
    Generating a RSA private key
    ...................+++++
    ........................+++++
    unable to write 'random state'
    writing new private key to 'client-key.pem'
    -----
    Certificates initialized
    MySQL init process in progress...
    2019-08-21T02:24:52.252131Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
    2019-08-21T02:24:52.255747Z 0 [Note] mysqld (mysqld 5.7.27) starting as process 88 ...
    2019-08-21T02:24:52.263003Z 0 [Note] InnoDB: PUNCH HOLE support available
    2019-08-21T02:24:52.263284Z 0 [Note] InnoDB: Mutexes and rw_locks use GCC atomic builtins
    2019-08-21T02:24:52.263371Z 0 [Note] InnoDB: Uses event mutexes
    2019-08-21T02:24:52.263450Z 0 [Note] InnoDB: GCC builtin __atomic_thread_fence() is used for memory barrier
    2019-08-21T02:24:52.263510Z 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
    2019-08-21T02:24:52.263610Z 0 [Note] InnoDB: Using Linux native AIO
    2019-08-21T02:24:52.267372Z 0 [Note] InnoDB: Number of pools: 1
    2019-08-21T02:24:52.268609Z 0 [Note] InnoDB: Using CPU crc32 instructions
    2019-08-21T02:24:52.276799Z 0 [Note] InnoDB: Initializing buffer pool, total size = 128M, instances = 1, chunk size = 128M
    2019-08-21T02:24:52.290811Z 0 [Note] InnoDB: Completed initialization of buffer pool
    2019-08-21T02:24:52.295286Z 0 [Note] InnoDB: If the mysqld execution user is authorized, page cleaner thread priority can be changed. See the man page of setpriority().
    2019-08-21T02:24:52.308149Z 0 [Note] InnoDB: Highest supported file format is Barracuda.
    2019-08-21T02:24:52.324904Z 0 [Note] InnoDB: Creating shared tablespace for temporary tables
    2019-08-21T02:24:52.325206Z 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
    2019-08-21T02:24:52.398986Z 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
    2019-08-21T02:24:52.401856Z 0 [Note] InnoDB: 96 redo rollback segment(s) found. 96 redo rollback segment(s) are active.
    2019-08-21T02:24:52.402203Z 0 [Note] InnoDB: 32 non-redo rollback segment(s) are active.
    2019-08-21T02:24:52.403101Z 0 [Note] InnoDB: Waiting for purge to start
    2019-08-21T02:24:52.453827Z 0 [Note] InnoDB: 5.7.27 started; log sequence number 2625438
    2019-08-21T02:24:52.454365Z 0 [Note] Plugin 'FEDERATED' is disabled.
    2019-08-21T02:24:52.464556Z 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
    2019-08-21T02:24:52.469083Z 0 [Note] InnoDB: Buffer pool(s) load completed at 190821  2:24:52
    2019-08-21T02:24:52.470721Z 0 [Note] Found ca.pem, server-cert.pem and server-key.pem in data directory. Trying to enable SSL support using them.
    2019-08-21T02:24:52.471523Z 0 [Warning] CA certificate ca.pem is self signed.
    2019-08-21T02:24:52.479948Z 0 [Warning] Insecure configuration for --pid-file: Location '/var/run/mysqld' in the path is accessible to all OS users. Consider choosing a different directory.
    2019-08-21T02:24:52.491399Z 0 [Note] Event Scheduler: Loaded 0 events
    2019-08-21T02:24:52.491928Z 0 [Note] mysqld: ready for connections.
    Version: '5.7.27'  socket: '/var/run/mysqld/mysqld.sock'  port: 0  MySQL Community Server (GPL)
    Warning: Unable to load '/usr/share/zoneinfo/iso3166.tab' as time zone. Skipping it.
    Warning: Unable to load '/usr/share/zoneinfo/leap-seconds.list' as time zone. Skipping it.
    Warning: Unable to load '/usr/share/zoneinfo/zone.tab' as time zone. Skipping it.
    Warning: Unable to load '/usr/share/zoneinfo/zone1970.tab' as time zone. Skipping it.
    mysql: [Warning] Using a password on the command line interface can be insecure.
    mysql: [Warning] Using a password on the command line interface can be insecure.
    mysql: [Warning] Using a password on the command line interface can be insecure.
    mysql: [Warning] Using a password on the command line interface can be insecure.

    2019-08-21T02:24:55.866009Z 0 [Note] Giving 0 client threads a chance to die gracefully
    2019-08-21T02:24:55.866234Z 0 [Note] Shutting down slave threads
    2019-08-21T02:24:55.866333Z 0 [Note] Forcefully disconnecting 0 remaining clients
    2019-08-21T02:24:55.866413Z 0 [Note] Event Scheduler: Purging the queue. 0 events
    2019-08-21T02:24:55.866609Z 0 [Note] Binlog end
    2019-08-21T02:24:55.867567Z 0 [Note] Shutting down plugin 'ngram'
    2019-08-21T02:24:55.867722Z 0 [Note] Shutting down plugin 'partition'
    2019-08-21T02:24:55.867800Z 0 [Note] Shutting down plugin 'BLACKHOLE'
    2019-08-21T02:24:55.867866Z 0 [Note] Shutting down plugin 'ARCHIVE'
    2019-08-21T02:24:55.867923Z 0 [Note] Shutting down plugin 'PERFORMANCE_SCHEMA'
    2019-08-21T02:24:55.868033Z 0 [Note] Shutting down plugin 'MRG_MYISAM'
    2019-08-21T02:24:55.868098Z 0 [Note] Shutting down plugin 'MyISAM'
    2019-08-21T02:24:55.868164Z 0 [Note] Shutting down plugin 'INNODB_SYS_VIRTUAL'
    2019-08-21T02:24:55.868223Z 0 [Note] Shutting down plugin 'INNODB_SYS_DATAFILES'
    2019-08-21T02:24:55.868277Z 0 [Note] Shutting down plugin 'INNODB_SYS_TABLESPACES'
    2019-08-21T02:24:55.868338Z 0 [Note] Shutting down plugin 'INNODB_SYS_FOREIGN_COLS'
    2019-08-21T02:24:55.868410Z 0 [Note] Shutting down plugin 'INNODB_SYS_FOREIGN'
    2019-08-21T02:24:55.868480Z 0 [Note] Shutting down plugin 'INNODB_SYS_FIELDS'
    2019-08-21T02:24:55.868551Z 0 [Note] Shutting down plugin 'INNODB_SYS_COLUMNS'
    2019-08-21T02:24:55.868619Z 0 [Note] Shutting down plugin 'INNODB_SYS_INDEXES'
    2019-08-21T02:24:55.868677Z 0 [Note] Shutting down plugin 'INNODB_SYS_TABLESTATS'
    2019-08-21T02:24:55.868748Z 0 [Note] Shutting down plugin 'INNODB_SYS_TABLES'
    2019-08-21T02:24:55.868798Z 0 [Note] Shutting down plugin 'INNODB_FT_INDEX_TABLE'
    2019-08-21T02:24:55.868883Z 0 [Note] Shutting down plugin 'INNODB_FT_INDEX_CACHE'
    2019-08-21T02:24:55.868940Z 0 [Note] Shutting down plugin 'INNODB_FT_CONFIG'
    2019-08-21T02:24:55.869009Z 0 [Note] Shutting down plugin 'INNODB_FT_BEING_DELETED'
    2019-08-21T02:24:55.869076Z 0 [Note] Shutting down plugin 'INNODB_FT_DELETED'
    2019-08-21T02:24:55.869132Z 0 [Note] Shutting down plugin 'INNODB_FT_DEFAULT_STOPWORD'
    2019-08-21T02:24:55.869209Z 0 [Note] Shutting down plugin 'INNODB_METRICS'
    2019-08-21T02:24:55.869291Z 0 [Note] Shutting down plugin 'INNODB_TEMP_TABLE_INFO'
    2019-08-21T02:24:55.869373Z 0 [Note] Shutting down plugin 'INNODB_BUFFER_POOL_STATS'
    2019-08-21T02:24:55.869450Z 0 [Note] Shutting down plugin 'INNODB_BUFFER_PAGE_LRU'
    2019-08-21T02:24:55.869531Z 0 [Note] Shutting down plugin 'INNODB_BUFFER_PAGE'
    2019-08-21T02:24:55.869613Z 0 [Note] Shutting down plugin 'INNODB_CMP_PER_INDEX_RESET'
    2019-08-21T02:24:55.869707Z 0 [Note] Shutting down plugin 'INNODB_CMP_PER_INDEX'
    2019-08-21T02:24:55.869780Z 0 [Note] Shutting down plugin 'INNODB_CMPMEM_RESET'
    2019-08-21T02:24:55.869850Z 0 [Note] Shutting down plugin 'INNODB_CMPMEM'
    2019-08-21T02:24:55.869900Z 0 [Note] Shutting down plugin 'INNODB_CMP_RESET'
    2019-08-21T02:24:55.869984Z 0 [Note] Shutting down plugin 'INNODB_CMP'
    2019-08-21T02:24:55.870042Z 0 [Note] Shutting down plugin 'INNODB_LOCK_WAITS'
    2019-08-21T02:24:55.870107Z 0 [Note] Shutting down plugin 'INNODB_LOCKS'
    2019-08-21T02:24:55.870174Z 0 [Note] Shutting down plugin 'INNODB_TRX'
    2019-08-21T02:24:55.870232Z 0 [Note] Shutting down plugin 'InnoDB'
    2019-08-21T02:24:55.870413Z 0 [Note] InnoDB: FTS optimize thread exiting.
    2019-08-21T02:24:55.870740Z 0 [Note] InnoDB: Starting shutdown...
    2019-08-21T02:24:55.971318Z 0 [Note] InnoDB: Dumping buffer pool(s) to /var/lib/mysql/ib_buffer_pool
    2019-08-21T02:24:55.971890Z 0 [Note] InnoDB: Buffer pool(s) dump completed at 190821  2:24:55
    2019-08-21T02:24:57.514213Z 0 [Note] InnoDB: Shutdown completed; log sequence number 12464449
    2019-08-21T02:24:57.517701Z 0 [Note] InnoDB: Removed temporary tablespace data file: "ibtmp1"
    2019-08-21T02:24:57.518178Z 0 [Note] Shutting down plugin 'MEMORY'
    2019-08-21T02:24:57.518375Z 0 [Note] Shutting down plugin 'CSV'
    2019-08-21T02:24:57.518568Z 0 [Note] Shutting down plugin 'sha256_password'
    2019-08-21T02:24:57.518758Z 0 [Note] Shutting down plugin 'mysql_native_password'
    2019-08-21T02:24:57.519439Z 0 [Note] Shutting down plugin 'binlog'
    2019-08-21T02:24:57.520625Z 0 [Note] mysqld: Shutdown complete


    MySQL init process done. Ready for start up.

    2019-08-21T02:24:57.774999Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
    2019-08-21T02:24:57.777231Z 0 [Note] mysqld (mysqld 5.7.27) starting as process 1 ...
    2019-08-21T02:24:57.781710Z 0 [Note] InnoDB: PUNCH HOLE support available
    2019-08-21T02:24:57.781915Z 0 [Note] InnoDB: Mutexes and rw_locks use GCC atomic builtins
    2019-08-21T02:24:57.781977Z 0 [Note] InnoDB: Uses event mutexes
    2019-08-21T02:24:57.782066Z 0 [Note] InnoDB: GCC builtin __atomic_thread_fence() is used for memory barrier
    2019-08-21T02:24:57.782134Z 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
    2019-08-21T02:24:57.782178Z 0 [Note] InnoDB: Using Linux native AIO
    2019-08-21T02:24:57.782541Z 0 [Note] InnoDB: Number of pools: 1
    2019-08-21T02:24:57.782770Z 0 [Note] InnoDB: Using CPU crc32 instructions
    2019-08-21T02:24:57.784717Z 0 [Note] InnoDB: Initializing buffer pool, total size = 128M, instances = 1, chunk size = 128M
    2019-08-21T02:24:57.794206Z 0 [Note] InnoDB: Completed initialization of buffer pool
    2019-08-21T02:24:57.797425Z 0 [Note] InnoDB: If the mysqld execution user is authorized, page cleaner thread priority can be changed. See the man page of setpriority().
    2019-08-21T02:24:57.809507Z 0 [Note] InnoDB: Highest supported file format is Barracuda.
    2019-08-21T02:24:57.824732Z 0 [Note] InnoDB: Creating shared tablespace for temporary tables
    2019-08-21T02:24:57.825065Z 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
    2019-08-21T02:24:57.889105Z 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
    2019-08-21T02:24:57.891664Z 0 [Note] InnoDB: 96 redo rollback segment(s) found. 96 redo rollback segment(s) are active.
    2019-08-21T02:24:57.892144Z 0 [Note] InnoDB: 32 non-redo rollback segment(s) are active.
    2019-08-21T02:24:57.893181Z 0 [Note] InnoDB: Waiting for purge to start
    2019-08-21T02:24:57.943908Z 0 [Note] InnoDB: 5.7.27 started; log sequence number 12464449
    2019-08-21T02:24:57.944714Z 0 [Note] Plugin 'FEDERATED' is disabled.
    2019-08-21T02:24:57.951108Z 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
    2019-08-21T02:24:57.956700Z 0 [Note] Found ca.pem, server-cert.pem and server-key.pem in data directory. Trying to enable SSL support using them.
    2019-08-21T02:24:57.957208Z 0 [Warning] CA certificate ca.pem is self signed.
    2019-08-21T02:24:57.959882Z 0 [Note] InnoDB: Buffer pool(s) load completed at 190821  2:24:57
    2019-08-21T02:24:57.960699Z 0 [Note] Server hostname (bind-address): '*'; port: 3306
    2019-08-21T02:24:57.960985Z 0 [Note] IPv6 is available.
    2019-08-21T02:24:57.961139Z 0 [Note]   - '::' resolves to '::';
    2019-08-21T02:24:57.961244Z 0 [Note] Server socket created on IP: '::'.
    2019-08-21T02:24:57.977164Z 0 [Warning] Insecure configuration for --pid-file: Location '/var/run/mysqld' in the path is accessible to all OS users. Consider choosing a different directory.
    2019-08-21T02:24:57.990196Z 0 [Note] Event Scheduler: Loaded 0 events
    2019-08-21T02:24:57.990639Z 0 [Note] mysqld: ready for connections.
    Version: '5.7.27'  socket: '/var/run/mysqld/mysqld.sock'  port: 3306  MySQL Community Server (GPL)
    ```

7. Deployment Aplikasi Web

    ```
    $ kubectl get pods
    NAME                            READY   STATUS    RESTARTS   AGE
    bukutamu-app-7d68b78568-j4bmr   1/1     Running   0          49s
    bukutamu-db-78b9445cbf-4dfnq    1/1     Running   0          3m20s
    

    $ kubectl logs bukutamu-app-7d68b78568-j4bmr

    .   ____          _            __ _ _
    /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
    ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
    \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
    '  |____| .__|_| |_|_| |_\__, | / / / /
    =========|_|==============|___/=/_/_/_/
    :: Spring Boot ::        (v2.1.5.RELEASE)

    2019-08-21 02:27:10.219  INFO 1 --- [           main] c.m.e.t.a.a.AplikasiBukutamuApplication  : Starting AplikasiBukutamuApplication v0.0.1-SNAPSHOT on bukutamu-app-7d68b78568-j4bmr with PID 1 (/opt/app.jar started by root in /)
    2019-08-21 02:27:10.240  INFO 1 --- [           main] c.m.e.t.a.a.AplikasiBukutamuApplication  : No active profile set, falling back to default profiles: default
    2019-08-21 02:27:14.106  INFO 1 --- [           main] .s.d.r.c.RepositoryConfigurationDelegate : Bootstrapping Spring Data repositories in DEFAULT mode.
    2019-08-21 02:27:14.426  INFO 1 --- [           main] .s.d.r.c.RepositoryConfigurationDelegate : Finished Spring Data repository scanning in 277ms. Found 1 repository interfaces.
    2019-08-21 02:27:15.849  INFO 1 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'org.springframework.transaction.annotation.ProxyTransactionManagementConfiguration' of type [org.springframework.transaction.annotation.ProxyTransactionManagementConfiguration$$EnhancerBySpringCGLIB$$56d071f9] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
    2019-08-21 02:27:17.009  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
    2019-08-21 02:27:17.163  INFO 1 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
    2019-08-21 02:27:17.168  INFO 1 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.19]
    2019-08-21 02:27:17.462  INFO 1 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
    2019-08-21 02:27:17.468  INFO 1 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 6974 ms
    2019-08-21 02:27:18.126  INFO 1 --- [           main] o.f.c.internal.license.VersionPrinter    : Flyway Community Edition 5.2.4 by Boxfuse
    2019-08-21 02:27:18.162  INFO 1 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Starting...
    2019-08-21 02:27:19.782  INFO 1 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Start completed.
    2019-08-21 02:27:19.787  INFO 1 --- [           main] o.f.c.internal.database.DatabaseFactory  : Database: jdbc:mysql://bukutamu-db-service/bukutamudb (MySQL 5.7)
    2019-08-21 02:27:20.178  INFO 1 --- [           main] o.f.core.internal.command.DbValidate     : Successfully validated 3 migrations (execution time 00:00.078s)
    2019-08-21 02:27:20.213  INFO 1 --- [           main] o.f.c.i.s.JdbcTableSchemaHistory         : Creating Schema History table: `bukutamudb`.`flyway_schema_history`
    2019-08-21 02:27:20.374  INFO 1 --- [           main] o.f.core.internal.command.DbMigrate      : Current version of schema `bukutamudb`: << Empty Schema >>
    2019-08-21 02:27:20.378  INFO 1 --- [           main] o.f.core.internal.command.DbMigrate      : Migrating schema `bukutamudb` to version 1.0.0.2019061901 - Skema Awal
    2019-08-21 02:27:20.460  INFO 1 --- [           main] o.f.core.internal.command.DbMigrate      : Migrating schema `bukutamudb` to version 1.0.0.2019061902 - Email harus unik
    2019-08-21 02:27:20.598  INFO 1 --- [           main] o.f.core.internal.command.DbMigrate      : Migrating schema `bukutamudb` to version 1.0.0.2019071801 - Tambahan Field Tamu
    2019-08-21 02:27:20.736  INFO 1 --- [           main] o.f.core.internal.command.DbMigrate      : Successfully applied 3 migrations to schema `bukutamudb` (execution time 00:00.526s)
    2019-08-21 02:27:21.154  INFO 1 --- [           main] o.hibernate.jpa.internal.util.LogHelper  : HHH000204: Processing PersistenceUnitInfo [
        name: default
        ...]
    2019-08-21 02:27:21.395  INFO 1 --- [           main] org.hibernate.Version                    : HHH000412: Hibernate Core {5.3.10.Final}
    2019-08-21 02:27:21.402  INFO 1 --- [           main] org.hibernate.cfg.Environment            : HHH000206: hibernate.properties not found
    2019-08-21 02:27:21.855  INFO 1 --- [           main] o.hibernate.annotations.common.Version   : HCANN000001: Hibernate Commons Annotations {5.0.4.Final}
    2019-08-21 02:27:22.614  INFO 1 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.MySQL5Dialect
    2019-08-21 02:27:24.211  INFO 1 --- [           main] j.LocalContainerEntityManagerFactoryBean : Initialized JPA EntityManagerFactory for persistence unit 'default'
    2019-08-21 02:27:26.011  INFO 1 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
    2019-08-21 02:27:26.234  WARN 1 --- [           main] aWebConfiguration$JpaWebMvcConfiguration : spring.jpa.open-in-view is enabled by default. Therefore, database queries may be performed during view rendering. Explicitly configure spring.jpa.open-in-view to disable this warning
    2019-08-21 02:27:27.214  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
    2019-08-21 02:27:27.224  INFO 1 --- [           main] c.m.e.t.a.a.AplikasiBukutamuApplication  : Started AplikasiBukutamuApplication in 18.609 seconds (JVM running for 19.889)
    ```

8. Melihat daftar services

    ```
    $ kubectl get svc
    NAME                  TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
    bukutamu              LoadBalancer   10.245.183.88   <pending>     80:30932/TCP   2m21s
    bukutamu-db-service   ClusterIP      10.245.168.48   <none>        3306/TCP       4m17s
    kubernetes            ClusterIP      10.245.0.1      <none>        443/TCP        34m

    $ kubectl get svc
    NAME                  TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)        AGE
    bukutamu              LoadBalancer   10.245.183.88   157.230.197.5   80:30932/TCP   3m6s
    bukutamu-db-service   ClusterIP      10.245.168.48   <none>          3306/TCP       5m2s
    kubernetes            ClusterIP      10.245.0.1      <none>          443/TCP        35m
    ```

## Scale out dan Replication

1. Scale aplikasi web menjadi 10 instances

    ```
    $ kubectl scale deployment.v1.apps/bukutamu-app --replicas=10
    deployment.apps/bukutamu-app scaled
    
    $ kubectl get pods
    NAME                            READY   STATUS              RESTARTS   AGE
    bukutamu-app-7d68b78568-5tx26   0/1     ContainerCreating   0          3s
    bukutamu-app-7d68b78568-64hpz   0/1     ContainerCreating   0          3s
    bukutamu-app-7d68b78568-97m7l   0/1     ContainerCreating   0          3s
    bukutamu-app-7d68b78568-c27q6   0/1     ContainerCreating   0          3s
    bukutamu-app-7d68b78568-fqfx4   0/1     ContainerCreating   0          3s
    bukutamu-app-7d68b78568-glw6p   0/1     ContainerCreating   0          3s
    bukutamu-app-7d68b78568-j4bmr   1/1     Running             0          13m
    bukutamu-app-7d68b78568-ppkpj   0/1     ContainerCreating   0          3s
    bukutamu-app-7d68b78568-qnsh2   0/1     ContainerCreating   0          3s
    bukutamu-app-7d68b78568-z5f5c   0/1     ContainerCreating   0          3s
    bukutamu-db-78b9445cbf-4dfnq    1/1     Running             0          15m
    ```

2. Enable autoscaling

    ```
    $ kubectl get hpa
    No resources found.

    $ kubectl get deployments
    NAME           READY   UP-TO-DATE   AVAILABLE   AGE
    bukutamu-app   10/10   10           10          32m
    bukutamu-db    1/1     1            1           35m

    $ kubectl autoscale deployment bukutamu-app --cpu-percent=50 --min=1 
    --max=10
    horizontalpodautoscaler.autoscaling/bukutamu-app autoscaled

    $ kubectl get deployments
    NAME           READY   UP-TO-DATE   AVAILABLE   AGE
    bukutamu-app   10/10   10           10          33m
    bukutamu-db    1/1     1            1           35m

    $ kubectl get hpa
    NAME           REFERENCE                 TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
    bukutamu-app   Deployment/bukutamu-app   <unknown>/50%   1         10        0          10s
    ```