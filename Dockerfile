FROM openjdk:8-alpine
ADD target/aplikasi-bukutamu-0.0.1-SNAPSHOT.jar /opt/app.jar
RUN sh -c 'touch /opt/app.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/app.jar"]